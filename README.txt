This is Sharon and Aashrita's Othello game!

Note: 
Please refer to player_old2.cpp for definitely working code (if player.cpp does
not end up working well). Also player_old.cpp just holds the oldest working
version, which doesn't necessarily win.

Note that we made our first commits prior to Sunday at midnight :)

Assignment 2:

1. We both split the work relatively equally. Sharon worked on Minimax and 
Aashrita worked on Heuristic and Alpha Beta. Sharon also spent a significant 
time debugging code.

2. We spent a significant amount of time trying to get our week 1 functions to 
work properly. We started out with a basic implementation using random 
numbers, and moved on to also examining all points around an opponent's piece. 
Then we implemented heuristics and became more selective with choosing our 
moves by weighing each spot on the board. We then also expanded our earlier 
algorithm such that we were examining every available spot around all of the 
opponent's peice (rather than just the last piece they put down, like we had 
earlier). 

We then implemented a depth 2 minimax that was hard coded (rather than 
implemented recursively). This week, we expanded that to take any n depths and 
check the best move recursively. With this general implementation, we were 
able to easily implement alpha-beta pruning by simply adding a few more lines 
that selectively cut off certain branches. 

Yay! Passing! We love CS2! :D