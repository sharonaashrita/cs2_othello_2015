#include "player.h"
#include <stdio.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     // set up board
     b = new Board();
     mySide = side;
     // do precalculations

}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete b;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */

    int timeLeft = msLeft;
    // NOT SURE HOW TO KEEP TRACK OF THE TIME
    while (timeLeft > 0 || msLeft == -1)
    {
        // PROCESS OPPONENT'S MOVE
        // Update board to reflect opponent's move
        if (opponentsMove != NULL)
        {
            Side opponent_side;
            if (mySide == WHITE)
            {
                opponent_side = BLACK;
            } 
            else
            {
                opponent_side = WHITE;
            }
            b->doMove(opponentsMove, opponent_side);
        }
        
        // PROCESS MY MOVE
        
        // Check if game over
        if (!b->isDone())
        {
            // Check if opponent made a move
            if (opponentsMove != NULL)
            {
                // Get all legal moves surrounding opponent's last move
                std::vector<Move *> all_pieces;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        int temp_x = opponentsMove->getX() - 1 + i;
                        int temp_y = opponentsMove->getY() - 1 + j;
                        if (temp_x >= 0 && temp_y >= 0 && temp_x <= 7 && temp_y <= 7)
                        {
                            Move *temp = new Move(temp_x, temp_y);
                            // Weed out the illegal moves
                            if (b->checkMove(temp, mySide))
                            {
                                all_pieces.push_back(temp);
                            }
                            else {
                                int rand1_x = rand() % 8;
                                int rand1_y = rand() % 8;
                                Move *random1 = new Move(rand1_x, rand1_y);
                                if (b->checkMove(random1, mySide))
                                {
                                    b->doMove(random1, mySide);
                                    return random1;
                                }
                            }
                        }
                    }
                }
                // Just make the first legal move in the list for now
                if (all_pieces.size() != 0) {
                    b->doMove(all_pieces[0], mySide);
                    return all_pieces[0];    
                }
            }
            // Make a random move for now
            else if (b->hasMoves(mySide))
            {
                while (true)
                {
                    int rand_x = rand() % 8;
                    int rand_y = rand() % 8;
                    Move *random = new Move(rand_x, rand_y);
                    if (b->checkMove(random, mySide))
                    {
                        b->doMove(random, mySide);
                        return random;
                    }    
                }
            }
            else {
                return NULL;
            }
         }
         else {
            return NULL;
         }
         timeLeft--;  
    }     
    return NULL;
}
