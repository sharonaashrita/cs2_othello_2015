#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
#include <iostream>
using namespace std;

class Board {
   
private:
          
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
    
    bitset<64> black;
    bitset<64> taken; 
    
    int space_pts[8][8];
    
    bool occupied(int x, int y);
    bool onBoard(int x, int y);
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void setBoard(char data[]);
};

#endif
