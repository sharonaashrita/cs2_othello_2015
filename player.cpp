#include "player.h"
#include <stdio.h>

const int DEPTH = 5;
const int ALPHA = -20;
const int BETA = 20;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     // set up board
     board = new Board();
     my_side = side;
     // do precalculations

}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

std::vector<Move *>Player::getAllMoves(Board *b, Side side)
{
    bitset<64> other_bits;
    // Get all of the other player's pieces
    if (side == WHITE) {
        other_bits = b->black;
    }
    else {
        other_bits = b->taken ^ b->black;
    }    
    std::vector<Move *> moves;
    // Get all legal moves for the given side on the given board
    for (unsigned int k = 0; k < other_bits.size(); k++) {
        if(other_bits[k]) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    int temp_x = (k % 8) - 1 + i;
                    int temp_y = (k - (k % 8))/8 - 1 + j;

                    // Check if within board boundaries
                    if (b->onBoard(temp_x, temp_y)) {
                        Move *m = new Move(temp_x, temp_y);
                        // Weed out the illegal moves
                        if (b->checkMove(m, side))               
                            moves.push_back(m);
                    }
                }
            }
        }
    }
    return moves;
}

bests Player::minimax(Board *b, Side side, int al, int be, std::vector<Move*> moves, int depth)
{
    bests best;
    // Base case: return current board heuristic
    if (moves.size() == 1) {
        best.score = b->space_pts[moves[0]->getX()][moves[0]->getY()];    
        best.move = moves[0];
        return best;
    }

    if (depth == 0) {
        if (side == my_side) {
            best.score = -10;
            for (unsigned int i = 0; i < moves.size(); i++) {    
                int score = b->space_pts[moves[i]->getX()][moves[i]->getY()];
                if (score > best.score) { 
                    best.score = score;
                    best.move = moves[i];
                }   
            }
        }
        else
        {
            best.score = 10;
            for (unsigned int i = 0; i < moves.size(); i++) {    
                int score = b->space_pts[moves[i]->getX()][moves[i]->getY()];
                if (score < best.score) {
                    best.score = score;
                    best.move = moves[i];
                }   
            }   
        }
        return best;
    }

    Side other_side = (side == BLACK) ? WHITE : BLACK;
    
    if (side == my_side) {
        best.score = -10;
        bests best_tmp;
        
        // Get best move based on all possible moves
        for (unsigned int i = 0; i < moves.size(); i++) {
            // Simulate making a move
            Board *b_tmp = new Board();                   
            b_tmp->doMove(moves[i], side);
            // Get best resulting opponent move
            std::vector<Move *> moves_tmp = getAllMoves(b_tmp, other_side);
            best_tmp = minimax(b_tmp, other_side, al, be, moves_tmp, depth - 1);
            
            if (best_tmp.score > best.score)
            {
                best.score = best_tmp.score;
                best.move = moves[i];
            }
            al = (al > best_tmp.score) ? al : best_tmp.score;
            
            if (be <= al)    
               break;

            delete b_tmp;
        }
    }
    else {
        best.score = 10;
        bests best_tmp;
        // Get best move based on all possible moves
        for (unsigned int i = 0; i < moves.size(); i++) {
            // Simulate making a move
            Board *b_tmp = new Board();

            b_tmp->doMove(moves[i], side);
            // Get best resulting opponent move
            std::vector<Move *> moves_tmp = getAllMoves(b_tmp, other_side);
            best_tmp = minimax(b_tmp, other_side, al, be, moves_tmp, depth - 1);            
            
            if (best_tmp.score < best.score)
            {
                best.score = best_tmp.score;
                best.move = moves[i];
            }
            
            be = (be < best_tmp.score) ? be : best_tmp.score;
            
            if (be <= al) 
               break;

            delete b_tmp;
        }
    }
    return best;
    
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */

    int timeLeft = msLeft;

    // NOT SURE HOW TO KEEP TRACK OF THE TIME
    while (timeLeft > 0 || msLeft == -1)
    {
        // PROCESS OPPONENT'S MOVE
        // Figure out who is on which side
        Side oppo_side = (my_side == BLACK) ? WHITE : BLACK;
        // Update board to reflect opponent's move
        board->doMove(opponentsMove, oppo_side);
    
        // PROCESS MY MOVE
        // Check if game over
        if (!board->isDone()) {
            // Determine all possible moves
            std::vector<Move *> moves = getAllMoves(board, my_side);
            bests theBest = minimax(board, my_side, ALPHA, BETA, moves, DEPTH);
            board->doMove(theBest.move, my_side);
            return theBest.move;
        }
        else {
            return NULL;
        }
        timeLeft--;  
    }     
    return NULL;
}
