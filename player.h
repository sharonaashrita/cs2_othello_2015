#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <cstdlib>
#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

struct bests {
    int score;
    Move *move;
};

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    std::vector<Move *>getAllMoves(Board *b, Side side);
    bests minimax(Board *b, Side side, int al, int be, std::vector<Move*> moves, int depth);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    // Board that the player is playing on
    Board *board;
    // Side that our AI is playing on (WHITE OR BLACK)
    Side my_side;
    
};

#endif
